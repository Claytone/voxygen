import sys
import time

from OpenGL.GL import *
from OpenGL.GLU import *
from OBJFileLoader.objloader import OBJ
import logging
import os
os.environ['PYGAME_HIDE_SUPPORT_PROMPT'] = "hide"
import pygame
from pygame.locals import *
log = logging.getLogger()


def view(filename):
    log.debug("Initializing pygame window...")
    start_window_load = time.time()

    pygame.init()
    pygame.display.set_caption(f"VoxyGen: Loading {os.path.split(filename)[1]}")

    try:
        logo_path = os.path.abspath("voxygen logo.PNG")
        icon = pygame.image.load(logo_path)
        pygame.display.set_icon(icon)
    except Exception as exc:
        log.warning(f"Failed to load `voxygen logo.PNG` because of {exc}")

    viewport = (800, 600)
    hx = viewport[0] / 2
    hy = viewport[1] / 2
    srf = pygame.display.set_mode(viewport, OPENGL | DOUBLEBUF)

    glLightfv(GL_LIGHT0, GL_POSITION, (-40, 200, 100, 0.0))
    glLightfv(GL_LIGHT0, GL_AMBIENT, (0.2, 0.2, 0.2, 1.0))
    glLightfv(GL_LIGHT0, GL_DIFFUSE, (0.5, 0.5, 0.5, 1.0))
    glEnable(GL_LIGHT0)
    glEnable(GL_LIGHTING)
    glEnable(GL_COLOR_MATERIAL)
    glEnable(GL_DEPTH_TEST)
    glShadeModel(GL_SMOOTH)  # most obj files expect to be smooth-shaded

    end_window_load = time.time()
    log.debug("Initialized window in %s seconds." % (end_window_load - start_window_load))

    # LOAD OBJECT AFTER PYGAME INIT
    start_obj_load = time.time()
    obj = OBJ(filename, swapyz=True)
    obj.generate()
    end_obj_load = time.time()
    log.info("Loaded %s in %s seconds." % (filename, end_obj_load - start_obj_load))
    start_dimension_load = time.time()
    max_dimensions = obj.get_bounding_box()[1]
    end_dimension_load = time.time()
    log.debug("Computed bounding box in %s seconds." % (end_dimension_load - start_dimension_load))
    offset_z = (-2 * max_dimensions[2]) - 5

    log.debug("Loading the rest of the window...")
    start_window_load = time.time()
    clock = pygame.time.Clock()

    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    width, height = viewport
    gluPerspective(45.0, width / float(height), 1, 150.0)
    glTranslatef(0, -2, offset_z)
    glRotated(20, 1, 0, 0)
    glEnable(GL_DEPTH_TEST)
    glMatrixMode(GL_MODELVIEW)

    end_window_load = time.time()
    log.debug("Finished making window in %s seconds." % (end_window_load - start_window_load))
    log.info("Loaded. Click and drag to navigate.")
    pygame.display.set_caption(f"VoxyGen: {os.path.split(filename)[1]}")

    rx, ry = (0, 0)
    tx, ty = (0, 0)
    zpos = 5
    rotate = move = False
    auto_rotate = True
    while 1:
        clock.tick(30)
        for e in pygame.event.get():
            if e.type == QUIT:
                return True
            elif e.type == KEYDOWN and e.key == K_ESCAPE:
                return True
            elif e.type == MOUSEBUTTONDOWN:
                auto_rotate = False
                if e.button == 4:
                    zpos = max(1, zpos - 1)
                elif e.button == 5:
                    zpos += 1
                elif e.button == 1:
                    rotate = True
                elif e.button == 3:
                    move = True
            elif e.type == MOUSEBUTTONUP:
                auto_rotate = True
                if e.button == 1:
                    rotate = False
                elif e.button == 3:
                    move = False
            elif e.type == MOUSEMOTION:
                i, j = e.rel
                if rotate:
                    rx += i
                    ry += j
                if move:
                    tx += i
                    ty -= j
        if auto_rotate:
            rx += 1
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        glLoadIdentity()

        # RENDER OBJECT
        glTranslate(tx / 20., ty / 20., - zpos)
        glRotate(ry, 1, 0, 0)
        glRotate(rx, 0, 1, 0)
        obj.render()

        pygame.display.flip()
