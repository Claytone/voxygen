import logging
import os
import pathlib
import time
from importlib import import_module

import yaml
from voxypy.models.models import Entity

log = logging.getLogger()
parent_path = str(pathlib.Path(__file__).parent.parent.resolve())
CONFIG_FILE = os.path.sep.join([parent_path, "config.yml"])
COMMANDS = {}

try:
    log.debug("Opening config file from %s" % os.path.sep.join([os.getcwd(), CONFIG_FILE]))
    with open(CONFIG_FILE) as stream:
        config = yaml.safe_load(stream)
    for command_name in config["valid_commands"]:
        module = import_module("voxygen.commands.%s" % command_name)
        COMMANDS[command_name] = getattr(module, command_name)
except Exception as e:
    log.error("Encountered %s while loading config file %s" % (e, CONFIG_FILE))
    raise


def execute(entity, command):
    # TODO: attempt to import methods at runtime and nix config file
    if command.name not in list(COMMANDS.keys()):
        raise IndexError("Unknown function name `%s`. Did you add it to %s?" % (command.name, CONFIG_FILE))
    log.info("Executing command: %s" % command.name)
    return COMMANDS[command.name](entity, **command.args)


def get_entity_from_dict(input_dict):
    # TODO: Allow custom module folder
    name = list(input_dict.keys())[0]
    contents = input_dict[name]
    validate_vxgn_dict(contents)
    entity = Entity(int(contents["x"]), int(contents["y"]), int(contents["z"]))
    if "texture_file" in contents.keys():
        entity.texture_file = contents["texture_file"]
    script = contents["script"]
    for raw_command in script:
        try:
            start_cmd_time = time.time()
            parsed_command = Command().from_dict(raw_command)
            entity = execute(entity, parsed_command)
            end_cmd_time = time.time()
            log.info(f"Finished in {end_cmd_time - start_cmd_time} seconds.")
        except IndexError as exc:
            log.error(f"{exc} while executing command `{raw_command}`. Skipping...")
            continue
    return entity


def get_entity_from_vxgn(filename):
    with open(filename) as file_stream:
        try:
            return get_entity_from_dict(yaml.safe_load(file_stream))
        except yaml.YAMLError as exc:
            log.error(f"{exc} while reading {filename}")
            raise exc


def validate_vxgn_dict(vxgn):
    required_keys = ["x", "y", "z", "script"]
    for key in required_keys:
        if key not in vxgn.keys():
            raise ValueError(f"Required key {key} not found in key list {vxgn.keys()}")


class Command:
    def __init__(self, name=None, args=None):
        self.name = name
        self.args = args

    def __getitem__(self, item):
        return self.args[item]

    def from_dict(self, d):
        if "name" not in list(d.keys()):
            raise IndexError(f"`name` keyword not found in command: {d}")
        self.name = d.pop("name")
        self.args = d
        return self
