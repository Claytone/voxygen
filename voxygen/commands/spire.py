from math import floor, ceil
from random import random

from .utils import increment_color


def spire(entity, base_x, base_y, growth_rate=0.8, width=1, decay_rate=0, decay_amount=1, color_start=1, color_offset=1):
    """
    :param entity: Entity to add spires to.
    :param base_x: X coord of the base of this spire.
    :param base_y: Y coord of the base of this spire.
    :param width: Base width of the spire.
    :param growth_rate: At each layer, likelihood spire will continue growing. If 1, all spires reach the ceiling.
    :param decay_amount: When decaying, reduce the width by this much.
    :param decay_rate: Chance of a spire to shrink width at every layer.
    :param color_start: Color of the base (0-255)
    :param color_offset: Increase color value by this much every layer.
    :type decay_amount: int
    :type base_x: int
    :type base_y: int
    :type width: int
    :type growth_rate: float
    :type decay_rate: float
    :type color_start: int
    :type color_offset: int
    """

    z = 0

    while random() < growth_rate and z < entity.z and width >= 1:
        # _x, _y is a cursor with base x, y at the center.
        # Floor/ceil deal with even widths.
        for _x in range(base_x - floor(width / 2), base_x + ceil(width / 2)):
            for _y in range(base_y - floor(width / 2), base_y + ceil(width / 2)):
                try:
                    entity.set(_x, _y, z, color_start)
                except IndexError:
                    continue
        if random() < decay_rate:
            width -= decay_amount
        z += 1
        color_start = increment_color(color_start, color_offset)
    return entity