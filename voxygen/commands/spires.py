from random import random

from voxygen.commands.spire import spire
from voxygen.commands.utils import generate_seeds


def spires(entity, spawn_rate=0.2, growth_rate=0.8, width=1, decay_rate=0, decay_amount=1, color_start=1, color_offset=1, distribution="random"):
    """
    :param entity: Entity to add spires to.
    :param width: Base width of the spire. (1 - 255)
    :param spawn_rate: At each (x, y), how likely is a spire to spawn? (0 - 1.0)
    :param growth_rate: At each layer, chance spire will continue growing. (0 - 1.0)
    :param decay_amount: When decaying, reduce the width by this much. (0-255)
    :param decay_rate: Chance of a spire to shrink width at every layer. (0 - 1.0)
    :param color_start: Color of the base (0-255)
    :param color_offset: Increase color value by this much every layer. (0 - 255)
    :type decay_amount: int
    :type spawn_rate: float
    :type width: int
    :type growth_rate: float
    :type decay_rate: float
    :type color_start: int
    :type color_offset: int
    """
    spire_bases = generate_seeds(entity, spawn_rate=spawn_rate, distribution=distribution)
    for base in spire_bases:
        entity = spire(entity, base_x=base[0], base_y=base[1], width=width, growth_rate=growth_rate,
                       decay_amount=decay_amount, decay_rate=decay_rate,
                       color_start=color_start, color_offset=color_offset)
    return entity
