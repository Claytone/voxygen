import random

from voxygen.commands.ray import ray
from voxygen.commands.utils import generate_seeds_3d


def parse_axis(axis):
    axis = axis.lower()
    if axis == "all":
        return "x", "y", "z"
    axes = []
    if "x" in axis:
        axes.append("x")
    if "y" in axis:
        axes.append("y")
    if "z" in axis:
        axes.append("z")
    if axes is None:
        raise ValueError(f"Unable to parse axes from value {axis}. Use: `axis: x, y, z` -or- `axis: all`")
    return axes


# Sweet baby
def rays(entity, color=1, spawn_rate=0.2, length=5, width=2, height=2, axis="all"):
    axes = parse_axis(axis)
    seeds = generate_seeds_3d(entity, spawn_rate)
    for seed in seeds:
        ax = random.choice(axes)
        entity = ray(entity, x=seed[0], y=seed[1], z=seed[2],
                     length=length, width=width, height=height, axis=ax)
    return entity
