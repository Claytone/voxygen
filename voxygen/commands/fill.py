from .utils import increment_color


def fill(entity, min_height=0, max_height=-1, starting_color=1, color_increment=0, destructive=False):
    if max_height == -1:
        max_height = entity.z

    color = starting_color
    for z in range(min_height, max_height):
        for x in range(entity.x):
            for y in range(entity.y):
                if destructive:
                    entity.set(x, y, z, color)
                elif not destructive and entity.get(x, y, z) == 0:
                    entity.set(x, y, z, color)
        color = increment_color(color, color_increment)
    return entity
