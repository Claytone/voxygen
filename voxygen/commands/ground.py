from voxygen.commands.fill import fill


def ground(entity, color=1, destructive=False):
    return fill(entity=entity, max_height=1, starting_color=color, destructive=destructive)
