from random import random
import numpy as np


def increment_color(color, increment):
    if color == 0:
        return 0
    color = (color + increment) % 256
    if color == 0:
        color = 1
    return color


def perlin(x, y):
    # permutation table
    p = np.arange(256, dtype=int)
    np.random.shuffle(p)
    p = np.stack([p, p]).flatten()
    # coordinates of the top-left

    xi = x.astype(int)
    yi = y.astype(int)
    # internal coordinates
    xf = x - xi
    yf = y - yi
    # fade factors
    u = fade(xf)
    v = fade(yf)
    # noise components
    n00 = gradient(p[p[xi] + yi], xf, yf)
    n01 = gradient(p[p[xi] + yi + 1], xf, yf - 1)
    n11 = gradient(p[p[xi + 1] + yi + 1], xf - 1, yf - 1)
    n10 = gradient(p[p[xi + 1] + yi], xf - 1, yf)
    # combine noises
    x1 = lerp(n00, n10, u)
    x2 = lerp(n01, n11, u)
    return lerp(x1, x2, v)


def lerp(a, b, x):
    "linear interpolation"
    return a + x * (b - a)


def fade(t):
    "6t^5 - 15t^4 + 10t^3"
    return 6 * t ** 5 - 15 * t ** 4 + 10 * t ** 3


def gradient(h, x, y):
    "grad converts h to the right gradient vector and return the dot product with (x,y)"
    vectors = np.array([[0, 1], [0, -1], [1, 0], [-1, 0]])
    g = vectors[h % 4]
    return g[:, :, 0] * x + g[:, :, 1] * y


def sigmoid_mod(x):
    return (1 + (1 / (1 + np.exp(-x)))) / 2


def generate_perlin(len_x, len_y):
    lin_x = np.linspace(0, 5, len_x, endpoint=False)
    lin_y = np.linspace(0, 5, len_y, endpoint=False)
    x, y = np.meshgrid(lin_x, lin_y)
    texture = perlin(x, y)
    # Normalize to [0, 1] instead of [-1, 1]
    texture = np.vectorize(lambda n: (n/2) + 0.5)(texture)
    return texture


def generate_seeds(entity, spawn_rate, distribution):
    if distribution == "random":
        return [(x, y) for x in range(entity.x) for y in range(entity.y) if random() < spawn_rate]
    elif distribution == "perlin":
        texture = generate_perlin(entity.x, entity.y)
        texture_clamped = np.vectorize(lambda n: int(n <= spawn_rate))(texture).astype(int)
        dense = list(zip(*texture_clamped.nonzero()))
        return dense
    else:
        raise ValueError(f"Invalid distribution type: {distribution}")


def generate_seeds_3d(entity, spawn_rate):
    """
    @param entity:
    @type entity: Entity
    @param spawn_rate:
    @return:
    """
    seeds = np.random.random((entity.x, entity.y, entity.z))
    seeds = np.vectorize(lambda n: int(n <= spawn_rate))(seeds).astype(int)
    dense = list(zip(*seeds.nonzero()))
    return dense
