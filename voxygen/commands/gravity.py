def drop(x, y, entity):
    current = 0
    for z in range(entity.z):
        if entity.get(x, y, z) != 0:
            entity.set(x, y, current, entity.get(x, y, z))
            entity.set(x, y, z, 0)
            current += 1


def gravity(entity):
    for x in range(entity.x):
        for y in range(entity.y):
            drop(x, y, entity)
    return entity
