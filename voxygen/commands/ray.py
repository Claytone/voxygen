from voxypy.models.models import Entity

AXES = {"x": 0,
        "y": 1,
        "z": 2
        }


def ray(entity, x=0, y=0, z=0, color=1, length=5, width=1, height=1, axis="x"):
    """
    @type entity: Entity
    @param x: The base of the ray
    @type x: int
    @param y: The base of the ray
    @type y: int
    @param z: The base of the ray
    @type z: int
    @param length: Length along axis
    @param width: Width along axis
    @type width: int
    @param height: Height along axis
    @param axis: 0, 1, or 2. x=0, y=1, z=2.
    @type axis: int
    """
    if axis.lower() not in AXES.keys():
        raise ValueError(f"Invalid axis {axis}. Choices are x, y, z.")
    axis = AXES[axis]
    dimensions = [0, 0, 0]
    non_axis_dimensions = [0, 1, 2]
    non_axis_dimensions.remove(axis)
    dimensions[axis] = length
    dimensions[non_axis_dimensions[0]] = width
    dimensions[non_axis_dimensions[1]] = height
    corner_1 = (x, y, z)
    corner_2 = (x + dimensions[0],
                y + dimensions[1],
                z + dimensions[2])
    dense = entity.get_dense()
    dense[corner_1[0]:corner_2[0], corner_1[1]:corner_2[1], corner_1[2]:corner_2[2]] = color
    entity = Entity().from_dense(dense)
    return entity
