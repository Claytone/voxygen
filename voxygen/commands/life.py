import numpy as np
from .utils import increment_color, generate_seeds
import logging
log = logging.getLogger()

formations = {
    "blinker": [[0, 1, 0], [0, 1, 0], [0, 1, 0]],
    "glider": [[1, 0, 1], [0, 1, 1], [0, 1, 0]],
    "pulsar": [
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0],
        [0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0],
        [0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0],
        [0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0],
        [0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0],
        [0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0],
        [0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    ]
}


def inject(layer, x, y, pattern_name, color):
    if pattern_name not in list(formations.keys()):
        raise IndexError("Key %s not found." % pattern_name)
    pattern = formations[pattern_name]
    for _x in range(len(pattern)):
        for _y in range(len(pattern[0])):
            layer[_x + x][_y + y] = pattern[_x][_y] * color
    return layer


def count_neighbors(layer, x, y):
    max_x = len(layer) - 1
    max_y = len(layer[0]) - 1
    count = 0
    for _x in range(x - 1, x + 2):  # for each neighboring cell
        for _y in range(y - 1, y + 2):
            try:
                if _x == x and _y == y:  # don't count yourself
                    continue
                elif _x < 0 or _y < 0:  # world bounds
                    continue
                elif _x > max_x or _y > max_y:  # world bounds
                    continue
                elif layer[_x][_y] != 0:
                    count += 1
            except IndexError:
                continue
    return count


def life_layer(old_layer, color):
    # TODO: z=0 should be n=1
    x_size = len(old_layer)
    y_size = len(old_layer[0])
    new_layer = np.zeros((x_size, y_size), dtype=int)
    for x in range(x_size):
        for y in range(y_size):
            neighbors = count_neighbors(old_layer, x, y)
            if old_layer[x][y] != 0:  # alive
                if neighbors < 2:
                    new_layer[x][y] = 0
                elif neighbors == 2 or neighbors == 3:
                    new_layer[x][y] = color
                elif neighbors > 3:
                    new_layer[x][y] = 0
            elif neighbors == 3:
                new_layer[x][y] = color
    return new_layer


def life(entity, spawn_rate=0.1, color_start=1, color_offset=1, inject_pattern="", distribution="random"):
    color = color_start
    seeds = generate_seeds(entity, spawn_rate, distribution)
    z = 0
    for seed in seeds:
        entity.set(seed[0], seed[1], z, color)
    if inject_pattern != "":
        old = entity.get_layer(0)
        entity.set_layer(0, inject(old, 5, 5, inject_pattern, color))
    while z < entity.z - 1:
        layer = life_layer(old_layer=entity.get_layer(z), color=color)
        z += 1
        entity.set_layer(z, layer)
        color = increment_color(color, color_offset)
    return entity
