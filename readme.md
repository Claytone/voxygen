# VoxyGen
### Intro
VoxyGen is a tool for creating, exporting, and rendering generative voxel art. 

Art is generated from scripts written in a simple domain-specific language. This means everyone, not just programmers, can use VoxyGen.

### Quickstart
1. Install Python (at least v3.6)
2. Download VoxyGen from its [GitLab repo](https://gitlab.com/Claytone/voxygen) and save it anywhere on your computer. (Coming soon: releases page with pre-compiled binaries)
3. Open a command prompt in the VoxyGen folder and enter `pip install -r requirements.txt`
4. Now try `python main.py -h` and if you see a friendly help message, you're good to go.
5. VoxyGen accepts `.vxgn` or similarly formatted files. The full specification can be viewed below. For now, create a file called `cake.vxgn` in the VoxyGen folder. Add these contents to it:
````
---
cake:
    x: 5
    y: 5
    z: 3
    script: [
        {
            name: spires,
            spawn_rate: 0.25,
            width: 1,
            color_start: 5,
            decay_rate: 0
        },
        {
            name: fill,
            starting_color: 1,
            min_height: 0,
            max_height: 2,
            color_increment: 1
        }
    ]
````
6. Finally, run `python main.py --texture "examples/palettes/db32.png" cake.vxgn`
7. Wait a few moments and voila, you should see something (very roughly) resembling a cake spinning in a new window. ![Fortunately, this one is not a lie.](docs/cake_test.png)

### Arguments
When invoking VoxyGen from the command line, you can supply arguments to change certain behaviors.

| Name | Description |
| --- | --- |
| -t, --texture | Overrides the texture file mentioned in your vxgn file. |
| -v, --view (coming soon)| Renders the final product in an OpenGL window. Useful for rapid development. |
| -o, --obj (coming soon)| .obj file to export. |
| -v, --vox (coming soon)| .vox file to export. |

### VXGN Specification
VoxyGen uses vxgn scripts to generate entities. A .vxgn file should follow yaml syntax.
````
---  # Must start with three hyphens on the first line
tiny:  # Title of the files to be created. This one will be "tiny.vox" and "tiny.obj"
    x: 1  # Dimensions of the entity to be created. This is required.
    y: 2
    z: 2
    texture: "examples/palettes/mintgreen.png"  # coming soon
    script: [  # A list of commands named "script"
        {  # Example of a fill command, including the arguments to "fill"
            name: fill,
            starting_color: 1,
            min_height: 0,
            max_height: 2,
            color_increment: 1
        }
    ]
````


### Commands
This is a table of the built-in commands in VoxyGen (so far)

| Command | Description
| --- | --- | 
| fill | Fills the available area with voxels of the given `color`, up to a certain height. |
| flip | Rotates the entity 180 degrees on the x axis. |
| ground | Fills the bottom layer of the file with `color` from input. |
| life | Runs [Conway's Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life) where each layer is the next iteration. |
| spire | Creates a highly configurable stack of voxels.
| spires | Calls `spire` many times on random coordinates. | 


#### Fill
![Filled water around the pyramid](docs/fill.png)

| Parameter | Description | Type | Default |
| --- | --- | --- | --- |
| min_height | Which height to start filling, where 0 is the bottom layer. | 0-255 | 0 (floor) |
| max_height | Which height to stop filling. -1 is interpreted as the entity's ceiling. | 0-255 | -1 (ceiling) |
| starting_color | Color to fill with. Use 0 to erase instead of filling. | 0-255 | 1 |
| color_increment | How many color indices to increase per layer. Use 0 for monochrome fill. | 0-255 | 0 |
| destructive | Whether to overwrite voxels in layers that are being filled. | True/False | False |

Example: 
````
{
    name: fill,
    starting_color: 1,
    min_height: 0,
    max_height: 2,
    color_increment: 1
}
````


#### Ground
![The result of a normal call to ground](docs/floor.png)

Ground is a special wrapper for `fill`. It simply calls `fill` with a max_height of 1. This is meant to be an example of how VoxyGen commands can reference and wrap each other. 

| Parameter | Description | Type | Default |
| --- | --- | --- | --- |
| color | Which color to fill the ground with. 0 for erasure. | 0 - 255 | 1 |
| destructive | Whether to replace the voxels already there. | True/False | False |

Example: 
````
{
    name: ground,
    color: 1,
    destructive: True
},
````


#### Spire(s)
![The result of a configured call to spires](docs/spires2.png)

Spire (singular) spawns one spire. Spire***s*** (plural) spawns many spires at random. 

| Parameter | Description | Type | Default |
| --- | --- | --- | --- |
| growth_rate | Chance that the spire will continue to grow at each Z level. A growth_rate of 1 will cause each spire to reach the ceiling. | 0-1.0 | 0.8 |
| width | Width of the base of the spire. | 1-255 | 1 |
| decay_rate | Chance of the spire to shrink in width at each Z level. If a spire would shrink smaller than 1 width, it stops growing. To remove tapering of spires, set decay to 0.  | 0-1.0 | 0
| decay_amount | When a spire decays, width is reduced by this much  | 1-255. Ideally less than width. | 1 |
| color_start | Starting color of the base layer. | 1-255 | 1 |
| color_offset | At each Z level, increment the chosen color by this amount. This can be used for a nice gradient effect. For monochromatic spires, set this to 0. | 0-255 | 1 |
| spawn_rate | Chance of a spire spawning at every given grid space on the bottom level. A spawn_rate of 1 will be solid. Spire***s*** only | 0-1.0 | 0.2 |
| base_x | X coordinate of the base of the spire. Spire (singular) | 0-1.0 | 0.2 |

Example: 
````
{
    name: spires,
    spawn_rate: 0.05,
    growth_rate: 0.8,
    width: 3,
    decay_rate: 0,
    color_start: 1,
    color_offset: 1
},
````


### Life
![The result of a configured call to life](docs/life.png)

Creates an instance of [Conway's Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life) from a random starting seed position. Every vertical layer is the next iteration of the game. 

| Parameter | Description | Type | Default |
| --- | --- | --- | --- |
| spawn_rate | Chance of a voxel spawning at the base layer. The base layer is considered the first generation of a game of life. | 0-1.0 | 0.1 |
| color_start | Starting color of the base layer. For erasure, set this to 0. | 0-255 | 1 |
| color_offset | At each Z level, increment the chosen color by this amount. This can be used for a nice gradient effect. For monochromatic formations, set this to 0. | 0-255 | 1 |
| inject_pattern | Injects a known starting pattern for interesting results. | blinker, glider, or pulsar. More to be added. Read about it [here](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life) | None |

Example:
````
{
    name: life,
    spawn_rate: 0.5,
    color_start: 1,
    color_offset: 2
}
````


### Rays
![The result of rays](docs/rays.png)

Creates "rays", which are lines of voxels. 

| Parameter | Description | Type | Default |
| --- | --- | --- | --- |
| spawn_rate | Chance of a ray spawning at every point in space. Recommended to use values less than 0.01 | 0-1.0 | 0.001 |
| color | Ray's color index. 0 for erasure. | 0-255 | 1 | 
| length | Length of ray along axis. | 0-255 | 1 |
| width | Width of ray perpendicular to axis. | 0-255 | 1 |
| height | Height of ray perpendicular to axis. | 0-255 | 1 |
| axis | Direction for rays to point. | `all`, or any combination of `xyz`. | `all` |

Example:
````
{
    name: rays,
    spawn_rate: 0.001,
    color: 1,
    length: 20,
    width: 1,
    height: 1,
    axis: xy
}
````


### Custom commands
Creating a custom command is easy. You'll need to create a `<name>.py` file containing a function named `<name>`. Place it in the `cmd` folder. The function must take an Entity as the first argument, and return an entity. You'll also need to add `<name>` to the `valid_commands` section of `config.yml`. 

See the included commands for examples. 


#### Future commands
| Command | Description
| --- | --- | 
| mod | Fills voxels where i % n = 0  |
| sin/cos/tan | Divide each dimension by its axis length to get a decimal value, then apply the chosen function to it. If greater than `threshold`, insert a voxel there. |


![The result of two calls to spires](docs/scripts.png)

### License
Copyright 2021 Clayton Wells (except where other authors are mentioned)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.