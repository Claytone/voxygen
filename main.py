import os
import sys
import argparse

from ezOBJviewer.ezobjviewer import view
from vox2obj.vox2obj import convert_to_obj

import logging
from logging import StreamHandler, Formatter

from voxygen.parsing import get_entity_from_vxgn

log = logging.getLogger()


def init_log():
    log.setLevel(logging.DEBUG)
    handler = StreamHandler(stream=sys.stdout)
    handler.setFormatter(Formatter(fmt='[%(asctime)s: %(levelname)s\t] %(message)s'))
    log.addHandler(handler)


def get_args():
    parser = argparse.ArgumentParser(description="A tool for the creation of generative Voxel art.")
    parser.add_argument("file", type=str, help="The .vxgn file to generate your model.")
    parser.add_argument("--texture", type=str, help="The .png file to color your model. Should be one row of pixels.")
    parser.add_argument("-c", "--clean", action='store_true', help="Remove files generated during run.")
    return parser.parse_args()


def main():
    generated_files = []
    args = get_args()
    file_name = os.path.splitext(args.file)[0]
    vox_file = file_name + ".vox"
    obj_file = file_name + ".obj"
    entity = get_entity_from_vxgn(args.file)
    if args.texture:
        entity.texture_file = args.texture
    entity.write(vox_file)
    generated_files.append(vox_file)
    # TODO: rather than save to disk and reload for viewing, just pass it in memory
    # TODO: same with .vox
    # TODO: give the option to export obj, or render
    generated_files.extend(convert_to_obj(vox_file, obj_file, entity.texture_file))
    view(obj_file)
    if args.clean:
        for f in generated_files:
            log.info(f"Cleaning {f}")
            try:
                os.remove(f)
            except Exception as e:
                log.warning(f"Failed to remove {f}: {e}")


if __name__ == "__main__":
    init_log()
    main()
