from unittest import TestCase

from voxygen.commands.life import count_neighbors, life_layer
import logging as log

log.basicConfig(level=log.DEBUG)


class TestLife(TestCase):
    def test_count_neighbors(self):
        layer = [[0, 1, 0],
                 [0, 2, 0],
                 [3, 5, 4]
                 ]

        neighbors = count_neighbors(layer, 0, 1)
        self.assertEqual(neighbors, 1)

        neighbors = count_neighbors(layer, 1, 1)
        self.assertEqual(neighbors, 4)

        neighbors = count_neighbors(layer, 2, 0)
        self.assertEqual(neighbors, 2)

        neighbors = count_neighbors(layer, 2, 2)
        self.assertEqual(neighbors, 2)

    def test_life_layer(self):
        old_layer = [
            [1, 1, 1],
            [1, 1, 0],
            [0, 0, 0]
        ]

        expected = [
            [1, 0, 1],
            [1, 0, 1],
            [0, 0, 0]
        ]

        actual = life_layer(old_layer, 1)
        eq = True
        for x in range(len(expected)):
            for y in range(len(expected[0])):
                if eq and actual[x][y] == expected[x][y]:
                    eq = True
                else:
                    eq = False
        self.assertTrue(eq, "Received \n%s\n but expected\n%s" % (actual, expected))
