from unittest import TestCase

import numpy as np

from voxygen.commands.spire import spire
from voxypy.models.models import Entity


class TestSpires(TestCase):

    def test_spire(self):
        x, y, z = 2, 2, 1
        entity = Entity(x, y, z)
        base_x, base_y = 1, 1
        entity = spire(entity, base_x, base_y, width=1, color_start=255, growth_rate=1, decay_rate=0)
        expected = np.zeros((x, z, y), dtype=int)
        expected[base_x][0][base_y] = 255
        print(entity.get_dense())
        print(expected)
        for _x in range(x):
            for _y in range(y):
                self.assertEqual(entity.get(_x, _y, 0), expected[_x][0][_y],
                                 f"Mismatch between {entity.get_layer(0)} and {expected}")
