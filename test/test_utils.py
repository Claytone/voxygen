from unittest import TestCase

import numpy as np
from voxypy.models.models import Entity

from voxygen.commands.utils import increment_color, generate_seeds, generate_perlin


def matrices_are_equal(m1, m2):
    return all([m1[x][y] == m2[x][y] for x in range(len(m1)) for y in range(len(m1[0]))])


class TestUtils(TestCase):
    def test_increment_color(self):
        self.assertEqual(0, increment_color(0, 1))
        self.assertEqual(2, increment_color(1, 1))
        self.assertEqual(255, increment_color(254, 1))
        self.assertEqual(1, increment_color(255, 1))
        self.assertEqual(29, increment_color(255, 30))

    def test_perlin(self):
        dims = (100, 100)
        texture = generate_perlin(*dims)
        self.assertTrue(not np.any(texture[texture < 0]),
                        f"Perlin noise should not contain negative values:\n{texture}")
        self.assertTrue(not np.any(texture[texture > 1]),
                        f"Perlin noise should not contain values greater than 1:\n{texture}")

    def test_generate_seeds_random(self):
        dims = (3, 4, 5)
        entity = Entity(*dims)
        bases = len(generate_seeds(entity=entity, spawn_rate=0, distribution="random"))
        expected = 0
        self.assertEqual(bases, expected)

        bases = len(generate_seeds(entity=entity, spawn_rate=1, distribution="random"))
        expected = dims[0] * dims[1]
        self.assertEqual(bases, expected)

        dims = (10, 10, 10)
        entity = Entity(*dims)
        bases = len(generate_seeds(entity=entity, spawn_rate=0.5, distribution="random"))
        self.assertTrue(0 < bases < dims[0] * dims[1])  # 9.77e-5% chance of failure

    def test_generate_seeds_perlin(self):
        dims = (3, 4, 5)
        entity = Entity(*dims)
        bases = len(generate_seeds(entity=entity, spawn_rate=0, distribution="perlin"))
        expected = 0
        self.assertEqual(bases, expected)

        bases = len(generate_seeds(entity=entity, spawn_rate=1, distribution="perlin"))
        expected = dims[0] * dims[1]
        self.assertEqual(bases, expected)