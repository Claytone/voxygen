from unittest import TestCase

import numpy as np
from voxypy.models.models import Entity

from test.test_utils import matrices_are_equal
from voxygen.commands.fill import fill


class TestFill(TestCase):
    def test_fill(self):
        x, y, z = 1, 2, 3
        entity = Entity(x, y, z)
        entity = fill(entity, max_height=1, starting_color=255)
        expected = np.zeros((x, y), dtype=int)
        for _x in range(x):
            for _y in range(y):
                expected[_x][_y] = 255
        self.assertTrue(matrices_are_equal(expected, entity.get_layer(0)))
        self.assertFalse(matrices_are_equal(expected, entity.get_layer(1)))
